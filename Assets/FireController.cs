﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour
{

    public float speed = 1f;
    private Vector3 direction = Vector3.right;
    private Rigidbody2D body;
    public GameObject player;


    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        direction = player.GetComponent<PlayerController>().GetDirection();
        transform.parent = null;
        //body.velocity = speed * direction; //commented this out
        Invoke("Die", 2f); // calls die after 2 seconds

    }
    
    private void Update() //added this function
    {
        transform.position = Vector3.Lerp(transform.position, transform.position + direction, speed * Time.deltaTime);


        
    }

    void Die()
    {
        if(gameObject != null)
            Destroy(gameObject);
    }
    //supposed to kill enemies, not sure why its not working
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
            Die();
    }
    

}
