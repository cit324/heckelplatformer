﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftRight : MonoBehaviour
{
    public LayerMask ground;
    private int direction = 1; //starts moving to the right
    private Rigidbody2D body;
    private SpriteRenderer rend;
    private Animator anim;
    private float h;
    private bool isFacingRight = true;
    
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        rend = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1f, ground);
        if(hit.collider == null) //no ground beneath the dragon
        {
           direction = direction * -1; //switch directions
           
        }
        transform.position = Vector2.Lerp(transform.position, new Vector2(transform.position.x + 1 * direction, transform.position.y), Time.deltaTime);

        

        // make player stop quicker
        if (Mathf.Abs(direction) < 0.5)
            direction = 0;

        // make sprite face correct direction
        if (direction > 0 && !isFacingRight) //moving to right
            flip(); //updated
        else if (direction < 0 && isFacingRight) //moving to left
            flip(); //updated

       

    }
    public Vector2 GetDirection()
    {
        if (isFacingRight) //added if/else here
            return Vector3.right;
        else
            return Vector3.left;
    }
    public void flip() //added this entire function
    {
        isFacingRight = !isFacingRight; // don't forget to update the flag!
        Vector3 theScale = transform.localScale;
        theScale.x = theScale.x * -1; //inverts the value
        transform.localScale = theScale;
    }

    void Die()
    {
        if (gameObject != null)
            Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Shot"))
            Die();
    }

}
